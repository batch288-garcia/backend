// console.log("Hello");

// [Section] Objects 
	/*
		- a object is a dat type tha tis used to represent real world objects.
		- it is a collection f related data and/or functionalities.
	
		Structure of Objects:
			- key - is what we call the property of an object
			- value - is the value of the specific ket or property.

	*/

// Creaeting using the object initializers/ object literal notation.
	// Syntax:
		/*
			let objectName = {
	
				keyA: ValueA,
				keyA: ValueB,
				 . . .
			}
		*/

 	let cellphone = {
 		// key-value pairs
 		name: 'Nokia 3210',
 		manufactureDate: 1999
 	} 	

 	// let exampleArray = [1, 2, 3, 4, 5];

 	// console.log(typeof exampleArray);

 	console.log('Result from creating objects using initializers?literal noation');
 	console.log(cellphone);

 // Creating objects using constructor function
 	// Creates a reusable function to create several objects that have he same data sreucture.
 	// Thhis is useful for creating multiple instances/copies of an object.

 	/*
		Syntax:
			function ObjectName(valueA, valueB){
				this.keyA = valueA;
				this.keyB = valueB;
			}
 	*/

// The "this" keyword allows us to assign a new objects properties by associating them with values recieved from a constructor's function parameters.
function Laptop(name, manufactureDate){
	this.name = name;
	this.manufactureDate = manufactureDate;
}
// this is a unique instance f the Laptop object
/*
	- the "new" operator creates a ninstance of an object
	- objects and instances are often interchanged becaue object literals (let object={}) and instance (let obkect = new obeject) are distinct/unique objects
*/

let laptop = new Laptop('Lenovo', 2008);
console.log('Result from creating object constructions: ');
console.log(laptop);

// this is another unique instance of the Laptop object
let myLaptop = new Laptop('MacBook Air', 2020);
console.log('Result from creating object constructions: ');
console.log(myLaptop);

let oldLAptop = Laptop('Portal R2E CCMC', 1980);
/*
	- The example above invokes/calss the "Laptop" function isnteadn of creating a new object instance
	- Return "undifiend" w/o the "new" operator because he "Laptop" function does not have a return statement
*/
console.log('Result from creating object constructions: ');
console.log(oldLAptop);

// Creating empty objecrt
let computer = {};
let myComputer = new Object();

// [Section] Accessing Object Properties

// Using the dot notation
console.log('Result from dot notation: ' + myLaptop.name);

// Using square bracket notation
console.log('Result from square bracket notation: ' + myLaptop['name']);

// Accessing array objects
/*
	- Accessing array elements can also be done using square brackets
	- Accessing object properties using the square bracket notation and array indexes can cause confusion
	- By using dot notation, this easily hlps s differentiate accessing elemens form arrays and properties from objects
	- Objects properties have names that make it easier to associate pieces of information
*/

let array = [laptop, myLaptop]

// May be confused for accessing array indexes
console.log(array[0]['name']);
// differentiate between accessing arrays and objects properties
// This tells us that array[0] is an object by using dot notation
console.log(array[0].name);

// [Section] Initializing/adding/Deleting?Reassigning Object Properties
/*
	- Like any ohther variable in JavaScript, object may have their properties initialized/add after the object was created/delcared
	- This is useful for times when an object's properties are undermined at the time of creating them
*/

let car = {};

// Initializing/adding object properties usng dot notation
car.name = 'Honda Civic';
console.log('Result from adding properties using dor notation');
console.log(car);

// Initializing/adding object properties using square bracket notation
/*
	- while using square bracket it iwll allow access to spaces when assigning property names to make it easier to read, this also makes it so that object properties can only be accessed using the square bracket notation
	- this also makes names of object properties to not follow commonly used naming conventions for them
*/

car['manufacture date'] = 2019;
console.log(car['manufacture date']);
console.log(car['Manufacture Date']);
console.log(car.manufactureDate);
console.log('Result from adding properties using square bracket notation');
console.log(car);

// Deleting object properties 
delete car['manufacture date'];
console.log('Result from deleting properties');
console.log(car);

// Reassiging object properties
car.name = 'Dodge Charger R/T';
console.log("Result from reassigning properties");
console.log(car);

// [Section] Object Methods
/*
	- A method is a function which is a property if an object
	- They are also functions and one of the key differences they have is that methods are function related to a specific object
	- Methods are useful for creating object specific functions which are used to perform tasks on them
	- Similar to functions/features of real world objects, methods are defined base on what an object is capable of oung and how it should work
*/

let person = {
	name: 'John',
	talk: function(){
		return 'Hello my name is ' + this.name;
	}
}

console.log(person);
console.log('Result from object methods: ');
console.log(person.talk());

// Adding methods to objects
person.walk = function(){
	console.log(this.name + ' walked 25 steps forward')
};
person.walk();

// Methods are usfle for creating reusale functions that perform tasks related to objects

let friend = {
	friendName: 'Joe',
	lastName: 'Smith',
	address: {
		city: 'Austin',
		state: 'Texas'
	},
	emails: ['joe@mail.com', 'joesmith@mail.xyz'],
	introduce: function(){
		console.log('Hello my name is ' + this.friendName + ' ' + this.lastName);
	}
}

friend.introduce();

// [Section] Real world application of ebjects
/*
	Scenario
		1. We would like to create a game that woud have several pokemon interact w/ each other
		2. Every pokemon would have the same set of stats, properties and functions
*/

// Using object literals to create multiple kids of pokemin would be time consuming 
let myPokemon = {
	name: "Pikachu",
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log("This polemon tackeld targetPokemon");
		console.log("targetPokemon's health is npw reduces to _targetPokemonhealth_");
	},
	faint : function(){
		console.log('Pokemon fainted');
	}
}

console.log(myPokemon);

// Creating an object constructor will help in ths process
function Pokemon(name, level) {
	
	// properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level

	// methods
	this.tackle = function(target){
		console.log(this.name + ' tackeld ' + target.name);
		console.log("targetPokemon's health is now reduced to _targetPokemonhealth_");
	};
	this.faint = function(){
		console.log(this.name + 'fainted')
	}
}

// Create new instance of "Pokemon" object each their unique properties
let pikachu = new Pokemon("Pikachu", 16);
let rattata = new Pokemon("Rattata", 8);

// Providing the "rattata" object as an argument to tha "pikachu" tacjle method will create interaction between the two objects. 
pikachu.tackle(rattata);



// console.log('Hello Player!');

// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals
let trainer = { 

// Initialize/add the given object properties and methods

// Properties
	name: 'Ash Ketchum',
	age: 10,
	pokemon: [
				'Pikachu', 
				'Charrizard', 
				'Squirtle',
				'Bulbasaur'
		 	],
	friends: {
			hoenn:	["May", "Max"],
			kanto: ["Brock", "Misty"]
	},

// Methods
	talk: function(){
		return this.pokemon[0] + "! I choose you!";
	}



};


// Check if all properties and methods were properly added

console.log(trainer);

// Access object properties using dot notation
console.log("Result of dot notation: ");
console.log(trainer.name);

// Access object properties using square bracket notation
console.log("Result of square bracket: ");
console.log(trainer['pokemon']);

// Access the trainer "talk" method
console.log("Result of talk method: ");
console.log(trainer.talk());

// Create a constructor function called Pokemon for creating a pokemon

function Pokemon(name, level, health, attack){
	this.name = name;
	this.level = level;
	this.health = health;
	this.attack = attack;


	//methods

		this.tackle = function(target) {

		console.log(this.name + " tackled " + target.name);

		target.health = target.health - this.attack;

		console.log(target.name + "'s health is now reduced to " + target.health );

		if(target.health  <= 0){
			 this.faint(target);
		}
	};
	    this.faint = function(target) {
		console.log(target.name + " has fainted"); 
	}

}


// Create/instantiate a new pokemon
	let Pikachu = new Pokemon('Pikachu', 12, 24, 12);
	

// Create/instantiate a new pokemon
	let Geodude = new Pokemon('Geodude', 8, 16, 8);
	

// Create/instantiate a new pokemon
	let Mewtwo = new Pokemon('Mewtwo', 100, 200, 100);


console.log(Pikachu);
console.log(Geodude);
console.log(Mewtwo);


// Invoke the tackle method and target a different object
	Geodude.tackle(Pikachu);

		console.log(Pikachu); 


// Invoke the tackle method and target a different object

	Mewtwo.tackle(Geodude);	
		
		console.log(Geodude); 





//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}
// console.log("Hello World!");

	function printName() {
		let nickName = "Chris";

		console.log("Hi, " + nickName);
	}

	printName();


	// For other cases, functions can also process data directly passed into it instead of relying only on Global variables

	// Consider this function
		function printName(name){
				console.log("My name is " + name);
		}

		printName("Juana");

		printName("Chris");

	// variables can also be passed as an argument

		let sampleVariable = "Curry";

		let sampleArray = ["Davis", "Green", "Jokic", "Tatum"]
		printName(sampleVariable);
		printName(sampleArray);

	// One example of using the prameter and Argument.
		// functioName(number);

		function checkDivisibilityBy8(num) {
			let remainder = num % 8;
		
			console.log("The remiander of " + num + " divided by 8 is: " + remainder);

			let isDivisibleBy8 = remainder === 0;

			console.log("Is " + num + " divisible by 8");
			console.log(isDivisibleBy8);
		};

		checkDivisibilityBy8(64);

		// Function as arguments
			// Function parameters can also accept other functions as arguments.

		function argumentFunction() {
			console.log("This function was passes as an argument before the message was printed!")
		}

		function invokeFunction(func) {
			func();
		}

		invokeFunction(argumentFunction);


	// functon as Argument with return keyword

		function returnFunction() {
			let name = "chris";
			return name;
		}

		function invokedFunction(func){
			console.log(func());
		}

		invokedFunction(returnFunction);

	// Using Multiple parameters

	// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.

		function createFullName(firstName, middleName, lastName) {
			console.log(firstName + " " + middleName + " " + lastName);
		}

		createFullName('Juan', "Dela", 'Cruz');

		// in JS, providing more/less arguments than the expected will not return an error

		// Providing less arguments than the expexcted will automatically assign undifiend value to the oparamaeters.

		createFullName('Gemar', null, 'Cruz');

		createFullName('Lito', "Masbate", "Galan", "Jr");

		// IOn other programming languags, this will return an error stauing "the number of arguments do not matc the number of parameter"

		// using variable as multiple arguments
		let firstName = "John";
		let middleName = "Doe";
		let lastName = "Smith";

		createFullName(firstName, middleName, lastName);

		// Using alert()
			// alert() allows us to show a small window at the top of ur browser page to show information to our users. As opposed to console.log which oly hows the message on the concole.

		// USing alert()

		/*alert("Hello World!"); // This will run immediatelt when the page loads 
		// Syntax: alert("messageInsString");*/
/*
		function showSampleAlert() {
			alert("Hello, user!");
		}

		// showSampleAlert();

		console.log("I will only log in the console when the alert is dismissed.");

		// Using prompt()
			// Prompt() allows us to show a small window at the top of the browser to gather user input
			// The value gathered from a prompt is returned as a string 
			// Syntax: prompt("dialogInString");
		let samplePrompt = prompt("Enter your name.");
		console.log(typeof samplePrompt);
		console.log("Hello, " + samplePrompt);

		let age = prompt("Enter your age.");
		console.log(typeof samplePrompt);
		console.log("Age: " + age);

		let sampleNullPrompt = prompt("Do not enter anything");
		console.log("Empty string: " + sampleNullPrompt);

*/

		function printWelcomeMessage(){
			let firstName = prompt("Enter your First Name");
			let lastName = prompt("Enter your last name");

			console.log("Hello, " + firstName + " " + lastName + "!");
			console.log("Welcome to my page");
		}

		printWelcomeMessage();
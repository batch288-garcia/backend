//Add code here

let http = require('http');

let app = http.createServer(function (request, response){

    if(request.url == "/" && request.method == "GET"){
        response.writeHead(200, {'Content-type' : 'application/json'});

        response.end("Welcome to Booking System");

    } else if(request.url == "/profile" && request.method == "GET"){
        response.writeHead(200, {'Content-type' : 'application/json'});

        response.end("Welcome to your profile!");

    } else if(request.url == "/courses" && request.method == "GET"){
        response.writeHead(200, {'Content-type' : 'application/json'});

        response.end("Here's our courses available");

    } else if(request.url == "/addcourse" && request.method == "GET"){
        response.writeHead(200, {'Content-type' : 'application/json'});

        response.end("Add a course to our resources");

    } else if(request.url == "/updatecourse" && request.method == "GET"){
        response.writeHead(200, {'Content-type' : 'application/json'});

        response.end("Update a course to our resources");

    }  else if(request.url == "/archivecourses" && request.method == "GET"){
        response.writeHead(200, {'Content-type' : 'application/json'});

        response.end("Archive course to our resources");
    }


}).listen(4000);

console.log("Server is running at localhost: 4000");


















//Do not modify
//Make sure to save the server in variable called app
if(require.main === module){
    app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;

// Advanced Queries

// Query an embedded documet / object

db.users.find({
	contact : {
		phone : "87654321",
		email : "stephenhawking@gmail.com"
	}
})

// will not work need to be complete object
db.users.find({
	contact : {
		email : "stephenhawking@gmail.com"
	}
});

// Dot notation to find item with single specific value
db.users.find({
	"contact.email" : "stephenhawking@gmail.com"
});

// Querying an array with exacy element
db.users.find({
	courses : ["CSS", "Javascript", "Python"]
});

// find array specific
db.users.find({
	courses : 
	{
		$all: ["React"]
	}
});

// 
db.users.find({
	courses : 
	{
		$all: ["React", "Sass"]
	}
});


// Query Operators

// [SECTION] Comparison Query Operators
	// $gt/gte operator 
		/*
			- allos us to find documents that have field number values greater or equal to a specified value
			- Note that this operator will only work if he data type of the field is number or integer.
			
			Syntax: 
				db.collectionName.find({field : {
						$gt : value
				}});

				db.collectionName.find({field : {
						$gte : value
				}});

		*/ 

// $gt
	db.users.find ({age : {$gt : 76}});
// $gte
	db.users.find ({age : {$gte : 72}});

// #lt/@lte operator
	/*
		- allows us to find documents that have field number values less than or equal to a specified value
		- Note: same with $gt/$gte operator, thios will only work if the data type of the field being queried is number or int.

		Syntax: 
			db.collectionName.find({field : {
					$lt : value
			}});

			db.collectionName.find({field : {
					$lte : value
			}});
	*/

//$lt
db.users.find({age : {$lt : 76}}); 

// $lte
db.users.find({age : {$lte : 76}}); 

// $ne operator
/*
	- allows us to find documents that have field number values not equal to sepcified value

	Syntax:
		db.collectionName.find({field : {$ne : value}});
*/

db.users.find({age : {$ne : 76}});

// $in operator
/*
	- allows us to find documents with specific match criteria one field using different valkues
	
	Syntax : 
		db.collectionName.find({field : {$in : value}});

*/

// $in opeartor
db.users.find({lastName : {$in : ["Hawking", "Doe"]}});

// $in opeartor in object
db.users.find({"contact.phone" : {$in : ["87654321"]}});

// $in opeartor in array
db.users.find({courses : {$in : ["React"]}});
// $in opeartor in multiple array array one of the value match it will return no need to match all criteria
db.users.find({courses : {$in : ["React", "Laravel"]}});

// [SECTION] Lofical QURY Operators
	// $or opeator
	/*
		- allows us to find documents that match a sngle criteria from multiple provided search criteria
	
		Syntax: 
			db.collectinName.find({
				$or : [{fieldA : valueA}, {fieldB : valueB}]
			})

	*/

// 
db.users.find({$or : [{firstName : "Neil"},{age : 25}]});

// add multiple opearators
db.users.find({$or : [{firstName : "Neil"},{age : {$gt : 25}}]});

// 
db.users.find({
	$or : 
	[{firstName : "Neil"},
	 {age : {$gt : 25}},
	 {courses : {$in : ["CSS"]}}
	]});

// $and Operator
	/*
		- allows us to find documents matching all multiple criteria in a single field 

		SYyntax:
			db.collectionName.find({
				$and : 
				[
					{fieldA : valueA},
					{fieldB : valueB}
					. . .
				]
			})
	*/

db.users.find({
	$and :
	[
		{age : {$ne : 82}},
		{age : {$ne : 76}}
	]
});

// Mini-Activity
	//  You are going to query all the documents from the users collection that follow these criteria:
		// Information o the documents that has age older than 30 that is enrolled in CSS or HTML.

db.users.find({
	$and : 
	[
		{age : {$gt : 30}},
		{courses : {$in : ["CSS" , "HTML"]}}
	]
});


// [SECTION] Field projection
	/*
		- retrieving documents are common operations that we do and by default mongoDB queriea return the whole document as a response.

	*/

// Inclusion
/*
	- allows us to include or add specific fields only when retrieving documents: 

	Syntax: 
		db.collectionName.find({criteria}, {field : 1})
*/

// display return documents on and off (0,1)
db.users.find(
	{firstName : "Jane"},
	{
		firstName : 1,
		lastName : 1,
		contact : 1,
		_id : 0
	}
);

// 
db.users.find(
	{firstName : "Jane"},
	{
		firstName : 1,
		lastName : 1,
		"contact.phone" : 1,
		_id : 0
	}
);

// Exclusion
/*
	- allows us to exclude.remove specific fields only when retrieving documents

	Syntax: 
		db.collectionName.find({criteria},{field : 0})

*/

db.users.find(
	{firstName : "Jane"},
	{	
		"contact.phone" : 0,
		department : 0
	}
);

// using logical operator

db.users.find(
	{ $or : 
		[
			{firstName : "Jane"},
			{age : {$gte :30}}
		]
	},
	{	
		"contact.phone" : 0,
		department : 0
	}
);

// 
db.users.find(
	{ $or : 
		[
			{firstName : "Jane"},
			{age : {$gte :30}}
		]
	},
	{	
		"contact.phone" : 0,
		department : 0,
		courses : {$slice : 2}
	}
);

// Evaluation Query Operators
	// $regex operator
		/*
			- allows us to find documents that match asoecific oattern using regular expressions.

			Syntax: 
				db.users.finf({
					field: $regex : 'pattern', option : 'optionValue'
				});
		*/
// Case Sensitive
db.users.find({firstName : {$regex: 'en'}});

// Case insensitive
db.users.find({firstName : {$regex: 'n', $options : 'i'}});
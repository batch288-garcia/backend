// console.log('Hello');

// Arrays Methods 
	// javaScript has built-in functions and methods for arrays. This allows us to manipulate and access array items.

	// Mutators Methods
	// Mutators mthods are functns that "mutate" or change an array after they'r created.
	// These methods manipulate the original array performing various such as adding or removing elements.

	let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

	// push()
		/*
			- adds an element in the end of an array and returns the updated array's length
			Syntax:
				arrayName.push();
		*/
	console.log("Current array: ");
	console.log(fruits);

	// push/add element at the end of array
	let fruitsLength = fruits.push('Mongo');

	console.log(fruitsLength);
	console.log('Mutated array from push method: ')
	console.log(fruits);

	// Pushing multiple elements toan array
	fruitsLength = fruits.push('Avocado', 'Guava');
	console.log(fruitsLength);

	console.log("Mutated array after pushing multiple elements: ");
	console.log(fruits);

	// pop()
	/*
		- removes the last element AND returns the removed element
		Syntax:
			arrayName.pop();
	*/

	console.log("Current Array: ");
	console.log(fruits);

	let removedFruit = fruits.pop();
	console.log(removedFruit);

	console.log("Mutated array from he pop method: ");
	console.log(fruits);

	// unshift()
	/*
		-it adds one or more elements at the beginning  of an array AND returns the updated array length
		Syntax:
			arrayName.unshift('elementA');
			arrayName.unshift('elementA', 'elementA' . . .);
	*/

	console.log("Current Array: ");
	console.log(fruits);

	fruitsLength = fruits.unshift('Lime', 'Banana');
	console.log(fruitsLength);

	console.log("Mutated array from unshift method: ")
	console.log(fruits);

	// shift()
	/*
		- removes an element at the beginning of an array AND returns the removed element.
		Syntax:
			arryaName.shift();

	*/

	console.log("Current array: ");
	console.log(fruits);

	removedFruit =  fruits.shift();
	console.log(removedFruit);

	console.log("Mutated array from the shift method: ")
	console.log(fruits);

	// spice()
	/*
		- Simultaneously removes an elements from a specified index number and adds element 
		- Syntax:
			arrayname.splice(startingIndex, deleteCount, elementToBeDAdded);
	*/

	console.log("Current array: ");
	console.log(fruits);

	// to delete index pos and number of deletion w/o adding new item
	// fruits.splice(4,2);

	// to delete index pos and number of deletion w/ replacement of new item
	// fruits.splice(4,2,"Lime", "Cherry");

	
	fruits.splice(fruits.length,0,"Cherry");

	console.log("Mutated array from the splice method: ")
	console.log(fruits);

	// sort()
		/*
			Rearranges the array elements in alphanumeric order
				syntax:
					arrayName.sort();
		*/

	console.log("Current array: ");
	console.log(fruits);

	fruits.sort();

	console.log("Mutated array from the sort method: ")
	console.log(fruits);

	// reverse()
		/*
			=reverse the order of aray elements
			Syntax:
				arrayName.reverse();
		*/

	console.log("Current array: ");
	console.log(fruits);

	fruits.reverse();

	console.log("Mutated array from the reverse method: ")
	console.log(fruits);


	// [Sectin] Non-mutator methods
	/*
		- Non-mutator methods are functions that do not modify or chnages an aray after the're created.
		- These methods do not manipulate the original array perfomring carious task such as returning elements fom an array and combining arrays and printing the output.
	
	*/

	let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

	// indexOf()
	// returns the index mumber of the first matching element in an array.
	// If no match was, found, the result will be -1.
	// THe search process will be done from the first element proceeding to the last element.
	/*
		Syntax:
			arrayName.indexOf(searchValue);
			arrayName.indexOf(searchValue, startingIndex);
	*/
	// w/ matching element in array
	let firstIndex = countries.indexOf('PH');
	console.log(firstIndex);
	//  w/o matching element in array
	let invalidCountry = countries.indexOf('BR');
	console.log(invalidCountry);

	firstIndex = countries.indexOf('PH', 2)
	console.log(firstIndex);

	console.log(countries);

	// lastIndexOf()
	/*
		- returns th index number of the last matching element fund in an array
		- the search process will be done from last element proceeding to the first element
		- If no match was, found, the result will be -1.
			Syntax:
				arrayName.lastIndexOf(searchValue);
				arrayName.lastIndexOf(searchValue, startingIndex);
	*/

	let lastIndex = countries.lastIndexOf('PH');
	console.log(lastIndex);

	invalidCountry = countries.lastIndexOf('BR');
	console.log(invalidCountry);

	// search start from indexnumber gooing to first element
	lastIndex = countries.lastIndexOf('PH', 6);
	console.log(lastIndex);


	// indexOf, starting from the starting index goping to the last element(from left to right);
	// lastIndexOf, starting from the starting index goping to the last element(from right to left);

	console.log(countries);

	// slice()
		/*
			- portions/slices elements form an array AND return a new array
				Syntax:
					arrayName.slice(startingIndex);
					arrayName.slice(startingIndex, endingIndex);
		*/

	// Slicing of element from a specfied index to the last element.

	let slicedArrayA = countries.slice(2);
	console.log("Result from slice method: ");
	console.log(slicedArrayA);

	// Slicing off elements from a spcified index to another index:
	// The elemen that will be sliced are elements from the starting index until the element before the ending index.
	let slicedArrayB = countries.slice(2,4);
	console.log("Result from slice method: ");
	console.log(slicedArrayB);

	// slicing off elements starting from the last element of an array:
	// return the number of elements indicated with "-" starting form the last element
	let slicedArrayC = countries.slice(-3);
	console.log("Result from slice method: ");
	console.log(slicedArrayC);

	// toString()
	/*
		return an array as string separated by commas
			Syntax:
				arrayName.toString();
	*/

	let stringArray = countries.toString();
	console.log("Result from toString method: ")
	console.log(stringArray);

	console.log(typeof stringArray);

	// concat()
	// combines arrays to an array or elements and returns the combined result.
	// Syntax:
		// arrayA.cooncat(arrayB);
		// arrayA.concat(elementA);

	let tasksArrayA = ["drink HTML", "eat javaScript"];
	let tasksArrayB = ["inhale CSS", "breathe sass"];
	let tasksArrayC = ["get git", "be node"];

	let tasks = tasksArrayA.concat(tasksArrayB);
	console.log(tasks);

	let combinedTasks= tasksArrayA.concat('smell express', 'throw react');
	console.log(combinedTasks);

	let allTasks= tasksArrayA.concat(tasksArrayC, tasksArrayB);
	console.log(allTasks);

	// concat array to an array and element
	let exampleTasks = tasksArrayA.concat(tasksArrayB, "smell express");
	console.log(exampleTasks);

// join()
	// retunr an array as string separated by specified separator string
	// Syntax:
		// arrayName.join("separatorString");

	let users = ['John', 'Jane', 'Joe', 'Robert'];

	console.log(users.join());
	console.log(users.join(''));
	console.log(users.join(" - "));

// [Section] Iteration Methods 
	// Iteration methods are loops designed to perform repititive task 
	// Iteration methods loops overall items in an array

	// forEach()
	// Similar to a for loop that iterates on each of array elements. 
	// Syntax:
		// arrayName.forEach(function(indivElement){statement};)

		console.log(allTasks);

		allTasks.forEach(function (task) {
			console.log(task);
		});
		// filteredYask variabel will hold all the elements from the alltask array that has mrope than 10 characters
	let filteredTasks = [];

	allTasks.forEach(function(task) {
		if(task.length > 10){
			filteredTasks.push(task);
		}
	});

	console.log(filteredTasks);


	// map()
	// Iterations on each element and returns new array with different values depending on the result of the function's operation

	let numbers = [1, 2, 3, 4, 5];

	let numberMap = numbers.map(function (number) {
		return number * number;
	});
	console.log(numbers);
	console.log(numberMap);

	// every()
	/*
		it will check if all elements in an array meet the given condition
		- return rue value if all elements meet the condition and false otherwise
		
		Syntax: 
			let/const resultarray = arrayName.every(funtion(indivElement){
				return expression/conditon;
			})

	*/

	numbers = [1, 2, 3, 4, 5];

	let allValid = numbers.every(function (number) {
			return (number<6);
	})

	console.log(allValid);


	// some()
	// checks if at least one element in the array meets the given condition.

	/*
		Syntax:
			let/const resultArray = arrayname.some(funcion(indivelement){
				return expression/condition
			})
	*/

	let someValid = numbers.some(function(number){
		return (number < 2);
	});

	console.log(someValid);

	// filter()
	// it returns new array that contains elements w/c meets the given
	// returns an empty array if no elements were found
	/*
		Syntax:
			let/const resultArray = arrayName.filter(funcion(indivelement){
				return expression/condition
			})
	*/

	numbers = [1, 2, 3, 4, 5];

	let filterValid = numbers.filter(function(number){
		return (number % 2 === 0);
	})

	console.log(filterValid);


	// includes()
		// checks if the argument passed can be found in the array

	let products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

	let productFound1 = products.includes('Mouse');
	console.log(productFound1);

	// reduce()
		// evaluates elements from left to right and returns/redices into single value

	numbers = [1, 2, 3, 4, 5];

	//The first parameter in the function will be accumulator
	// TThe second parameter in the fnction will be the currentValue 
	let reducedArray = numbers.reduce(function(x,y){
		console.log('Accumulator ' + x);
		console.log('currentValue ' + y);
		return x+y;
	});

	console.log(reducedArray);

	products = ["Mouse", "Keyboard", "Laptop", "Monitor"];

	let reducedArrayString = products.reduce(function(x,y){
		return x + " " + y;
	});

	console.log(reducedArrayString);
/*console.log('Hello');*/

// [Section] While Loop
	// A while loop takes an expression/condition. Expressions are any unit of code that can be evalauted as true or false. If the condition evaluates to be a true, the statements.code block will be executed.
	// A loop will iterate a certain number of times until an expression/condition is true
	// Iteration is the term given to repitition of statements.
	/*
		Syntax;
			while(expression/condition){
				statement;
				increment/decrement;
			}
	*/

let	count = 5;

	// While the value of count is not equal to zero the statement inside will run/interate
	while(count != 0){

		// The current value of count is printed out
		console.log("while " + count);
		
		// Decreases the value of count by 1 after every iteration to stop when it reaches 0.
		// Forgetting to include this in our loops wll make our applications run an infinite loop which will evetually crash our device.
		// After running the script, if a slow response form the browser is experienced or an infinite loop is seen in the console quickly close the application browser/tab to avoid this.
		count--;
	}

// [Section] Do while loop

	/*
		- a do while loop works a lot like the while loop. But unlike while loops, do-while loop guarantees that the code will be executed at least once.

		syntax:
			do{
				statement;
				increment/decrement;
			} while(condiition/expression)

	*/


/*	let number = Number(prompt("Give me a number: "));
		// The Number() works similarly to the "parseInt" funtion.

	do{
		console.log("Do while: " + number);

		// Increment
		number++;
	} while(number < 10);

*/

// [Section] For Loop
	/*
		A for loop is more flexible than while and do while loops. It consists of three parts:
			1. The "initialization" value that will track the progression of the loop.
			2. expression/condition that will be evaluated w/c will determine wheter the loop will run one more time.
			3. The "iteration" w/c indicates how to advance the loop.
			Syntax:
			for(initializaton; expression/condition; iteration){
				statemetn;
			}
	*/

	// Will create a loop that will start from0 and will end at 20
	// Every iteration of the loop, the value of count will be checked if it is equal or less than 20.
	// If the value of count is less than or equal 20 the statement inside the loop will execute.
	// The value of count will be incrementd by one for each iteration.
	// can also be counte += 2 add 2 to self
	// Will only be infinite if decrement
for(let count = 0; count <= 20; count +=2) {
	console.log("The current value of count is " + count);
}

let myString = "alex";
	// .length property
	// Characters in strings may be counted using the .length property.
console.log(myString.length);	

// Access elements of a string/ to the characters of the string
console.log(myString[0]);

	// We will create a loop that will print out the individual letters of the myString variable

	for(let index = 0; index < myString.length; index++ ){
		console.log(myString[index]);
	}

// Create a string named "myName" wigt vaue of Alex;

let myName = "Alex";
	
	/*
		Crete a loop that will print out the letter of the name individually and prinout the number 3 instead when the to be printed is vowel.
	*/


	for(let index = 0; index < myName.length; index++){
		// consolelog(myName[index]);
		if(myName[index].toLowerCase() === 'a' || 
			myName[index].toLowerCase() === 'e' || 
			myName[index].toLowerCase() === 'i' || 
			myName[index].toLowerCase() === 'o' || 
			myName[index].toLowerCase() === 'u'){
			console.log(3);
		}else{
			console.log(myName[index]);
		}
	}

	let myNickName = "Chis Mortel";

	let updateMyNickName = myNickName.replace("Chis", "Chris");

	console.log(myNickName);
	console.log(updateMyNickName);

console.log("-------------------------------------------------");

// [Section] Continue and Breal Statements
	// The "continue" statement allows the code to go to te next iteration of the loop w/o finishing the execution of all statements in a code block.
	// The "break" statement is used to terminate the current loop once a match has been found.
// Create a loop that if the count value is divided by and the remain o, it will print the number and continue to the next iteration of the loop.  

	for(let count = 0; count <= 20; count++){


		if (count >= 10) {
			console.log('The number is equal to 10 stopping');
			break
		}

		if(count % 2 === 0){
			console.log('The number is divisible to two, skipping...');
			continue;
		}
		console.log(count);

	}




	// create a loop that will iterate based on the length of the string name;
let name = "alexandro";
console.log("-------------------------------------------------");
	// We are going to console the letters per line, and if the letter is equal to "a" we are going to console ""Continue" to the next iteration" then add continue statement.
	// If the current letter is equal to "d" we are goping to stop the loop.

for(let index = 0; index < name.length; index++){

	if(name[index] === "a"){
		console.log("Continue to the next iteration!")
		continue;
	}else if (name[index] === "d"){
		break;
	}else{
		console.log(name[index]);
	}

}
// console.log("Hello World!");

// Fuinctions
	// Functios in JS are lines/block of codes that tell our device/application to perform a certain task when called/invoke
	// Function are mostly created to create complicated task to run several lines of code in successiom.

// Function declarations
	// defines a function with the specified parameters
	// Syntax:
		/*

			function functionName(){
				codeBlock/statements;
			}

		*/
	
	// function keyword - used to defined a JS function
	// functionName - the function name. Fnctions are named to be able to use later in the code for the invocation
	// function block ({}) - the statemens which compromise the body of the function. This is where the code will be executed/

		// Example
			function printName() {
				console.log("My name is John");
			};

// Function Invocation
	// The code block and stateents inside a function is not immediately executed when the function is defined/ The codeblock and statements inside the function is executed is invokde  or called.
	// It is common to use the term "call a function" instead of "invoke a function".

			// Let's invoke/ call the function that we declare.
			printName();

// --- Hoisted u can use even di pa na declare

// Function Decalaration vs Expression
	// Funcion Declaration
		// A function can be created through function declaration by busing the using the function keyword and adding the name.
		// Declared function/s can be hoisted.

		decalredFunction();

		function decalredFunction() {
			console.log("Hello World form decalredFunction!")
		};

	// Function Expression
		// A function can akso be stored in a variable.
		// A function expression is an anonymous function assigned to a variable function

		// example
		/*variableFunction();*/

		let variableFunction =  function(){
				console.log("Hello Again!");
		};

		variableFunction();

		// We can also create a function expression of a named function.

		let funcExpression = function funcName(){
			console.log("Hello form the other side!");		
		};

		funcExpression();

		// You can reassign declared functions and function expression to new anonymous function.

		decalredFunction = function(){
			console.log("Updated decalredFunction");
		};

		decalredFunction();

		funcExpression = function() {
			console.log("Updated funcExpression");
		};

		funcExpression();

		// However, we cannot re-assign a function expression initialized with const.

		const constantFunc = function() {
			console.log("Initialized with const");
		};

		constantFunc();

		/*constantFunc = function() {
			console.log("Can not be reassigned");
		};

		constantFunc();*/

// Function Scoping 
		// Scope it is the accessibility of variables within our program.

		/*
			JavaScript Variables has 3 types of scope:
			1. Global
			2. local/block scope
			3. function scope
		*/

		{

			let localVar = "Armando Perez";
		}

		// Result in error. localVarbeing in a bl;ock ot will not be accessible outside the blcok 
		// console.log(globalVar);

		let globalVar = "Mr. Worldwide";

		{

			console.log(globalVar);
		}

	// Function Scope
			// JavScript has function scope: Each function creates a new scope.
			// Variables defined omsode a function are not accessible (visible) from outside the function.

		function showName() {
			const functionConst = "John";
			let functionLet = "Jane";

			console.log(functionConst);
			console.log(functionLet);

		}

		// console.log(functionConst);
		// console.log(functionLet);

		showName();

	// Nested functions
		// /You can create another function inside a function. This is called anested function.

		function myNewFunction(){
			let name = "Jane";

			// nested function
			function nestedFunction(){
				console.log(name);
			}
			nestedFunction();
		}

		myNewFunction();

// Return Statement 	
	/*
		The return statement allows us to output a value froim a function to be passed to the line/bock of code that invoked/called the funtion.
	*/

		function returnFullName() {
			// u can store her arrays, objects
			let fullName = "Jeffrey Smith Bezos";

			let returnArray = [1, 2, 3, 4, 5];
			
			return returnArray;

			// after the retrn keyword below code will not run

			
		}
		// will not run with out console.log();
		returnFullName();
		
		console.log(returnFullName());


		let fullName = returnFullName();

		console.log(fullName);

// Function Naming Convensions\
		// Funtion namse should be definitive of the task it will perform. It usually contains a verb.

		function getCourses() {
			let courses = ["Science 101", "MAth 101", "English 101"]

			return courses;
		}


		// Avoid generic names to avoid confusion within our code.
		function get() {
			let name = "Jamie";

			return name;
		}

		// Avoid pointless and inappropirte function namses

		function foo() {
			return 25%5;
		}

		// Name ur function in small caps. Folow camelCame when nam,ing variables and functions w/ multiple words.

		function displayCarInfo(){
			console.log("Brand: toyoyta");
			console.log("Type: senda");
		}
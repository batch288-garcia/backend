// for show databases - list of the db inside our cluster
// use 'dbName' - to use a specific database
// show collectionss - to see the list of collections inside the db.

// help - to show codes in MongoDB
// cls - to clear terminal

// CRUD operation
/*
	- CRUD operations is the heart of any backend application.
	- mastering the CRUD operation is essential for any developer especially to hose whio want to become backend developer.
*/

// [SECTION] Inserting Document (Create)
	// Insert one document 
		/*
			- since mongoDB deals with objects as it's structure for documnents we can easily create them by providing objects in our method/operations.

			Syntax:
				db.collectionName.insertOne({
					object
				})

		*/

db.users.insertOne({
	firstName : "Jane",
	lastName : "Doe",
	age : 21,
	contact: {
		phone: "123456789",
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaSCript", "python"],
	department : "none"
});

// Insert Many

/*
	syntax:
		db.collectionName.insertMany([{objectA},{objectB}. . . ])
*/

db.userss.insertMany([
		{
			firstName : "Stephen",
			lastName : "Hawking",
			age : 76,
			contact :{
				phone: "87654321",
				email: "stephenhawking@gmail.com"
			},
			courses : ["Python", "React", "PHP"],
			department : "none"
		},

		{
			firstName : "Niel",
			lastName : "Armstrong",
			age : 82,
			contact : {
				phone : "87654321",
				email : "nielarmstrong@gmail.com"
			},
			course : ["React", "Laravel", "Sass"],
			department : "none"
		}
	]);

// [SECTION] Finding Documents (Read operation)
	// db.collectionName.find();
	// db.collectionName.find({field:value});

// Using the find() method, it will show you the list of all the documnets inside our collections.
db.users.find();

// The "pretty" methods allows us to be able to view the documents returned by or terminals to be in a better format.
db.users.find().pretty();

// it will return the documnets that will pass the criteria given in the method
db.users.find({firstName: "Stephen"});

// Find using ID
db.users.find({_id:
	ObjectId("646c59707f7996f4d2162c5e")});

// multiple criteria
db.users.find({firstName : "Armstrong", age:82});

// 
db.users.find({"contact.phone": "123456789"});

// 
db.users.find({contact : {phone: "123456789", email : "janedoe@gmail.com"}});

// [SECTION] Updating documnets (Update)
db.users.insertOne({
	firstName: "test",
	lastName : "test",
	age: 0,
	contact : {
		phone : "000000000",
		email : "test@gmail.com"
	},
	courses : [],
	department : "none"	

});

// UpdateOne Method
/*
	Syntax:
		db.collectionName.udpateOne({criteria, {$set: {field:value}}})
*/

db.users.updateOne(
	{firstName : "test"},
	{
		$set: {
			firstName: "Bill",
			lastName: "Gates",
			age: 65,
			contact : {
				phone : "123456789",
				email : "bill@gmail.com"
			},
			courses : ["PHP", "Laravel", "HTML"],
			department : "Operations",
			status : "none"
		}
	}
);\

// 

db.users.updateOne(
	{firstName : "Bill"},
	{
		$set: {
			firstName: "Chris"
		}
	}
);

// 
db.users.updateOne(
	{firstName : "Jane"},
	{
		$set: {
			lastName: "Edited"
		}
	}
);

// Updating multiple documnets
/*
	Syntax:
		db.collectionsName.updateMany(
			{criteria},
			{
				$set : {
					{field:value}
				}
			}
		);
*/

db.users.updateMany(
		{department: "none"},
		{$set : {
			department : "HR"
		}
	}
);

// Replace One
	/*
		Syntax:
			db.collectionName.repalceOne(
				{cirteria},
				{$set: {
					object
				}
			}
			)
	*/

db.users.insertOne({
	firstName: "test"
});

db.users.replaceOne(
	{firstName: "test"},
	{
		firstName : "Bill",
		lastName : "Gates",
		age : 65,
		contact : {},
		courses : [],
		department : "Operations"
		
	}
);

// [SECTION] Deleting Documents

// Deleting single document
	/*
		db.collectionName.deleteOne({criteria});
	*/

db.users.deleteOne({firstName : "Bill"});

// 
db.users.deleteMany({firstName : "Jane"});

// 
db.users.deleteMany({});

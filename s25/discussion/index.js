// console.log('Hello');

// [SECTION] JSON Objects
	// JSON stands for JavaScript Object Notation
	// JSON is also used in other programming languages hence the name JavaScript Object Notation.
	// Core JavaScript has a built in JSON object taht containes methods for parsing object and conerting strings into JS Onjects
	// .json file, JSON file
	// JavaScript objects are not t be confused with JSON
	// JSON is sued for serializibng different data types ito bytes
	// Serialization is the process of converting data into series of bytes for easier tranmission/ transfer of information/data.
	//  A byte is a unit of data that is eight binary digits (1 0) taht us used to represent a character (letter, numbers, typographic symbols).
	/*
		Syntax: 
		{
			"propertyA" : "valueA",
			"propertyB" : "valueB"
		}
	*/

// [SECTION] JSON Arrays 
	/*
		"cities" : [
			{"city" : "Quezon City", "province" : "Metro Manila", "country" : "Philippines"}
		]
	*/

// [Section] JSON Methods
	// The JSON object into stringified JSON.

	// Converting Data into Stringified JSON
		// Stringified JSON is a JS object converted into a string to be used n other functions of a JS application.
		// They are commonly used in HTTP request where information is required to ve sent and receive in a strignified version
		// Requests are an important part if programming where an application communicates with backend applpication to perform different tasks suchs as getting/creating data in a database.

	let batchesArr = [{batchNAme: 'Batch X'}, {batchName : 'Batch Y'}] 

	console.log(batchesArr); 
	console.log(typeof batchesArr); 

	// the stringify method is used to convert JS objects into a string
	console.log('Result from stringify method: ');
	// if u are going to convert objects to string, you'll use JSON.stringify(variableName);
	console.log(JSON.stringify(batchesArr));
	console.log(typeof JSON.stringify(batchesArr));

	// Using strinigfy methods w/ variables
		/*
			Syntax:
				JSON>strinify({
					propertyA: valueA,
					propertyB: valueB
				})
		*/

	// User details
/*	let firstName = prompt('What is your first name? ');
	let lastName = prompt('What is your last name? ');
	let age = prompt('what is your age? ');
	let address = {
		city: prompt('Which city do you live in? '),
		country: prompt('Which country does your city address belongs to? ')
	};
	let otherData = JSON.stringify({
		firstName: firstName,
		lastName: lastName,
		age: age,
		address: address
	});

	console.log(otherData);
*/
	// Converting stringified into JavaScript Objects 
	// Objects are common data typoes used in applications because of the complex data structures that can be created out of them

	let batchesJSON = '[{"batchName": "Batch X"}, {"batchName": "Batch Y"}]'

	console.log(batchesJSON);

	console.log('Result from parse method: ');
	console.log(JSON.parse(batchesJSON));

	let exampleObject = '{"name": "Chris", "age": 16, "isTeenager": false}'

	console.log(batchesJSON);

	console.log('Result from parse method: ');
	console.log(JSON.parse(exampleObject));

	let exampleParse = JSON.parse(exampleObject);

	console.log(typeof exampleParse.name);
	console.log(typeof exampleParse.age)
	console.log(typeof exampleParse.isTeenager);
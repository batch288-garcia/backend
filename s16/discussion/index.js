// To check wheter the script file is properly linked to ur html file

// console.log('Hello World');

// [Section] Arithmetic Operators
	let x = 1397;
	let	y = 7831;

	// Addition Operator(+)
	let sum = x + y;

	console.log("Result of addition operator: " + sum);

	// Subtraction Operators (-)
	let difference = y - x;

	console.log("Result of subtraction operator: " + difference);

	// Multiplication Operator (*)
	let prduct = x * y;

	console.log("Result of Multiplication operator: " + prduct);

	// Division Operator (/)
	let quotient = y / x;

	console.log("Result of division operator: " + quotient.toFixed(2));

	// Modulo Operator (%)
	let remainder = y % x;
	console.log("remainder of modulo operator: " + remainder);

// [Section] Assignment Operators

	// assignment operator (=)
	// The assignment operator assignsreassigns the value to the variable and assigns the variable

	let assignmentNumber = 8;

	// Addition Assignment Operator (+=)
	// The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.

	assignmentNumber += 2;

	console.log(assignmentNumber);

	assignmentNumber += 3;

	console.log("Result of addiiton assignment Operator: " + assignmentNumber);

	// Subtraction/multiplication/division assignment Operator(-=, *=, /=)

	// Subtraction Assignment Operator
	assignmentNumber -= 2;

	console.log("Result of subtraction assignement operator: " + assignmentNumber);

	// Multiplication Assignment Operator
	assignmentNumber *= 3;

	console.log("Result of Multiplication assignement operator: " + assignmentNumber);

	// Division Assignment Operator
	assignmentNumber /= 11;

	console.log("Result of division assignement operator: " + assignmentNumber);

// Multiple Operators and Parentheses
	// MDAS - Multiplication or division First then add or subtract, from left to right.
	let mdas = 1 + 2 - 3 * 4 /5;
	/*
		1. 3 * 4 = 12
		2. 12 / 5 = 2.4
		1 + 2 - 2.4
		3. 1 + 2 = 3
		4. 3. 2.4 = 0.6

	*/

	console.log(mdas.toFixed(1));

	let pemdas = 1 + (2-3) * (4/5);
	/*

		1. 4/5 = 0.8
		2. 2-3 = -1
		1 + (-1) * (0.8)
		3. -1 * 0.8 = -0.8
		4. 1 - 0.8
		0.2

	*/

	console.log(pemdas.toFixed(1));
	

// [Secrtion] Increment aand  Decrement 
	// Operators add or subtract values by 1 and reassigns the value of teh variable where the increment and decrement applied to.

	let z = 1;

	let increment = ++z;

	console.log("result od pre-increment: " + increment);

	console.log("Result of pre-increment: " + z);

	increment = z++;

	console.log("result od post-increment: " + increment);

	console.log("Result of post-increment: " + z);

	//  
	x = 1;

	let decrement = --x;

	console.log("Result of pre-decrement: " + decrement);
	console.log("Result of pre-decrement: " + x);

	decrement = x--;
	console.log("Result of post-decrement: " + decrement);
	console.log("Result of post-decrement: " + x);

// [Section] Type Coercin
	/*
		- Type of coercion is the automatic or implicit conversion of values friom one data type to another.
		- This happens when operations are performed on different datat types that would normally possible and yield irregular results.
	*/

	let numA = '10';
	let	numB = 12;

	let coercion = numA + numB;
	// IF u are going to add string and number, it will concatenate its value
	console.log(coercion);	
	console.log(typeof(coercion));

	let numC = 16;
	let numD = 14;

	let nonCoercion = numC  + numD;
	console.log(nonCoercion);
	console.log(typeof(nonCoercion));

	// The boolean "true" is associated with the value of 1
	let numE = true + 1;
	console.log(numE);
	// The boolean "false" is associated with the value of 0
	let	numF = false + 1;
	console.log(numF);


// [Section] Cpmparison Operators

	// 
	let juan = 'juan';

	// Equality Operator (==)
	/*
		- Checks wheter the operand are equal/have the same content/value.
		- Attemmpts to CONVERT AND COMPARE operands of different data types
		- Returns a Boolean Value
	
	*/

	console.log(1==1); // true value
	console.log(1 == 2); // false
	console.log(1 =='1'); // true
	console.log(0 == false); // true
	// compare two strings that are the same
	console.log('JUAN' == 'juan'); //false (Case-sensitivity)
	console.log(juan == 'juan') // True

	// Inequality Operator (!=)
	/*
		- checks wheter the operands are not equal/have different content/values.
		- Attemmpts to CONVERT AND COMPARE operands of different data types
		- return boolean value

	*/

	console.log(1 != 1); // false
	console.log(1 != 2); // true
	console.log(1 != '1'); // false
	console.log(0 != false); // false
	console.log('JUAN' != 'juan'); //true
	console.log(juan != 'juan'); // false

	// Strict Equality Operator(===)
	/*
		- Checks wheter the operands are equals.have the same content.
		- Also COMPARE the data types of two values.


	*/

	console.log(1 === 1); // true
	console.log(1 === 2); // false
	console.log(1 === '1'); // false
	console.log(0 === false); // false
	console.log('JUAN' === 'juan'); // false
	console.log(juan === 'juan'); // true

	// Strictly Inequlaity Operator(!==)
	/*
		- Check wheter the operands are not equal have different content
		Also COMPARES the data types of 2 values.
	*/


	console.log(1 !== 1); //false
	console.log(1 !== 2); // true
	console.log(1 !== '1'); // true
	console.log(0 !== false); // true
	console.log('JUAN' !== 'juan'); //true
	console.log( juan !== 'juan'); // false

// [Section] Relational Operators
	// Checks wheter one value is greater or less than to the other values.
	
	let	a = 50;
	let b = 65;	

	// GT pr greater than Operator (>)
	let isGreaterThan = a > b; //false
	// LT or Less Than Operator (<)
	let	isLessThan = a < b; //true
	// GTE pr Greater Than or Equal Operator (>=)
	a = 65;
	let isGTorEqual = a >= b; //false
	// LTE or Less Than or Equal (<=)
	let isLTorEqual = a <= b; //true

	console.log(isGreaterThan); // false
	console.log(isLessThan); // true
	console.log(isGTorEqual); // true
	console.log(isLTorEqual); // true

	let numStr = "30";
	console.log(a > numStr); // true
	console.log(b <= numStr); // false

	let strNum = "twenty";
	console.log(b >= strNum); // false
	// since the string is not numeric, the string was converted to a number and it resulted to NaN (Not a Number);

// [Section] Logical Operators 

	let isLegalAge = true;
	let isRegistered = false;

	// Logical AND operator (&& - double Ampersand)
	// Returns true if all operands are true.
	//isRegistered = true;
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of logical AND operator: " + allRequirementsMet);

	// Logical OR operator (|| - double pipe)
	// Returns true if one of the operands are true
	// isLegalAge = false;
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR operator: " + someRequirementsMet);

	// Logical NOT Operator (! - Exclamation Point)
	// Returns the opposite value.
	let someRequirementsNotMet = !isRegistered;
	console.log("Result of logical NOT Operator: " + someRequirementsNotMet);
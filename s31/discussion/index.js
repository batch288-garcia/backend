
// Use the "require" directive to load Node.js modules
// A module is a software component or part of a program that contains one or more routines
// THe "http" module lets node.js transfer data using the hypertext transpfer protocol
// In that way, HTTP is a protocol that allows the fetching of resources such as HTML documents


// Clietns (browser) and Servers (Node JS/Express JS applications) communicates by exchanging individual messages.
// These messages are sent by the clioents, usually a web browser and called "request"

// The messages sent by the server as an answer are called "response"
let http = require("http");

// Using this module's createServer() method, we can create an HTTP server that listens to request on a specified port and gives reponses back to the client.
// A Port is a virtual point where network connections starts and end.
// The http module has a creaeteServer() metod that accepts a function as an aguement and allows for creation of a server
// The arguement passes in the function are request and response objkects (data types) that contains methods that allows us to receive request from the client and send reposnses back to it.

// The server will be assigned to port 400 via the "listen(4000)" method where the server will listen to any request that are sent to it eventually communicating with our server.
http.createServer(function (request, response) {

	// Use the writeHead() method to: 
	// set a status code for the response - a 200 means OK 
	// Set the content-type of theresponse as a plain text messages
	response.writeHead(200, {'Content-Type' : 'text/plain'});

	// Send the reponse with the text content 'Hello World!'
	response.end('Hello World!');

}).listen(8000);

// When the server is running, console will print the message:
console.log('Server Running at localhost:8000');
// Use the "require" directive to load the express module/package
// It will allow us access methods and functions that will help us create easily our aplication or server.
// A 'module' is a software component or part of a program that contains one or more routines.
const express = require("express");

// create an applicationusing express 

// Creaets an express appciation and stores this in a constant variable called app.
const app = express();

// for our applicaiton server to runm, we need a port to listen
const port = 4000;

// Middlewares
	// Is software that provides common services and capabilities to applicaiton outside of what's ooffered by the operating System.

	// Allow our applicaiton to read json data
	app.use(express.json()); 

	// This one will llow us top read data from forms
	// By default, information received from the url can only be received as a string or an array
	
	// By applying the option of "extened : true" this will allow us to receive information inopther data types such as an object w/c will use throughout our application.
	app.use(express.urlencoded({extended: true}));

// [Section] Routes
	// Express has methods corresponding to each http method
	// This route expects to receive a GET request art the base URI "/"
	app.get("/", (request, response) => {
		// Once the route is accessed it will send a string reponse containing "Hello World!"

		// Compare to the previous session. .end ues the node JS module's method
		// .send method uses the expressJS module's method instead t send a response back to the client.

		response.send('Hello Batch 288!');

	});

	// This route expects to receive a GET request at the URI "/hello"
	app.get("/hello", function (request, response) {
		response.send('Hello form the /hello endpoinit');
	});

	// This route expects to receive a GET request at the URI "/hello"
	app.get("/hello", (request, response) => {
		request.send("Hello from the /hello endpoitn");
	});

	// This route expects to receive a POST request at the URI "/hello"
	app.post("/hello", (request, response) => {
		// request.body contains the contents/data of the request body
		// all the properties defined in our POSTMAN request will be accessibkle here as properties with the same name
		console.log(request.body);

		response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!` );
	});

	// An array will store user objects when the "/signup" route is accessed
	// This will serve as our mock database
	let users = [];

	app.post("/signup", (request, response) => {
		console.log(request.body);

		if(request.body.username !== "" && request.body.password !== ""){
			users.push(request.body);

			response.send(`User ${request.body.username} successfully registered`);
		}else{
			response.send('Please input both username and password.');
		}
	});

	// This route expects to receive a PUT request at the URI "/change-password"
	// This will update teh password of a yuser that matches the information provided in the client.postman
	app.put("/change-password", (request, response) => {
		let message;
			for(let index = 0 ; index < users.length; index++){

				// If the provided username in the client.postman and the username of the current object in the loop is the same
				if(request.body.username == users[index].username){
					
					users[index].password = request.body.password;

					message = `User ${request.body.username} password has been updated!`;

					break;
			}else {
				message = 'User does not exist!';
		}
		}
		response.send(message);

	});

	app.get("/home", (request, response) =>{
		response.send("Welcome to the home page");
	});

	app.get("/users", (request, response) => {
		if(users != ""){
			response.send(users)
			
		}else{
			response.send('There is no users store yet, please signup');
			
		}
	});

	app.delete("/delete-users", (request, response) => {
		let message;

		if(users != ""){
			for(let index = 0; index < users.length; index++){

				if(request.body.username == users[index].username){

					users.splice(index,1);

					message = `User ${request.body.username}'s has been deleted`

					break;
				}
			}
			if(message === undefined) message = `users des not exist`
	
		}
		else message = 'No users found'
	
		response.send(message);

	});



	// Tells our server to listen to the port
	// if the port is accessed, we can run the server
	// return a message confirming that the server is running in the terminal.
	if(require.main === module){
		app.listen(port, () => console.log(`Server running at port ${port}`));
	}

	module.exports = app;

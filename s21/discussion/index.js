/*console.log("Hello");*/
// An Array in programming is simply a list of data that shares the same data type.

let studentNumberA = "2020-1923";
let studentNumberB = "2020-1924";
let	studentNumberC = "2020-1925";
let	studentNumberD = "2020-1926";
let	studentNumberE = "2020-1927";

// Now, w/ an array, we can simply write the code above like this:

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'];

// [Section] Arrays
	// Arays are used to store multiple related values in a single variable.
	// They are declared using the square brackets([]) also known as "Array Loterals"

	/*
		Syntax:
			letconst arrayName = [ellementA, elementB, elementC . . . ];
	*/

let grades = [98.5, 94.3, 89.2, 90.1];
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Toshiba'];

// Possible use of an array but is not recommended
	let mixedArr = [12, 'Asus', null, undefined, {}];

console.log(grades);
console.log(computerBrands);
console.log(mixedArr);

let myTasks = [
		'drink html',
		'eat javascript',
		'inhale css',
		'bake saaa'
	];
console.log(myTasks);

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakarta";

let cities = [city1, city2, city3];

console.log(cities);

// city1 = "Osaka"

console.log(cities);

// [Section] length property
	// The .lenght property allows us to get and set the total number of items an array
console.log(myTasks.length);
console.log(cities.length);

let blankArr = [];
console.log(blankArr.length);

// .length property can also set the total number of elements in an array, meaning we can actually delete the last item in the array or shorten the array by imple updating the length of an array.

console.log(myTasks);

myTasks.length -= 1;

console.log(myTasks);
console.log(myTasks.length);

// To delete a specific item in an array we can employ array methods (w/c will be shown/discusses in the next session) or an algorithm.

// Another example using decrementation:
console.log(cities);
cities.length--;
console.log(cities);

// we cant do the same on strings
let fullName = "Jamie Noble";
console.log(fullName);
fullName.length -= 1;
console.log(fullName);
	// if u can shorten by setting the length property, u cannot shorten the numbers of chaacters using same property;

// Since u can shorten tha array by setting the length property, u can also legnthen it by adding a number insto the length property. Since we lengthen tah array facibly, there will be another item in the array, however it will be empty or undifined.

let theBeatles = ["John", "Ringo", "Paul", 'George'];
console.log(theBeatles);

// add empty arr at end
theBeatles.length += 1;
console.log(theBeatles);

// [Section] Reading from Arrays
	/*
		- Accesing array elements is one of the more common tasks that we can do with an array
		- We can do this by using the array indexes.
		- Each Element in an array is associated w/ its own index/numner.
		Syntax:
			arrayName[indexNumber];
	*/
console.log(grades[0]);

let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Kareem"];
console.log(lakersLegends);

// You can actually save/store array items in another variable

let currentLakers = lakersLegends[2];
console.log(currentLakers);

// You can also reassign values using items indices

console.log('Array Before reassignment: ');
console.log(lakersLegends);

// Reassign the value of specific elements:
lakersLegends[1] = "Pau Gasol";
console.log("Array after reassignment:")
console.log(lakersLegends);

// Accssing the last element of an array
// Since the first element of an array starts at o, suibsracting 1 to the length of an array will offset the val;ue by one allowing us to get the last element.
let bullsLegends = ["Jordan", "Pippen", "Rodman", "Rose", "Kukoc"];
console.log(bullsLegends[bullsLegends.length-1]);

// Adding element into the array

let newAArr = [];
console.log(newAArr);

newAArr[newAArr.length] = "Cloud Strife";
console.log(newAArr);

// reassigning arrays at end
newAArr[newAArr.length-1] = "Tifa LockHart";
console.log(newAArr);


console.log("--------------------------------------");


// [Section] Looping over an array
	// You can use a for loop to iterate over all items in an array

	// for loop
	for(let index =0; index < bullsLegends.length; index++){
		console.log(bullsLegends[index]);
	}

	//example:

		// We are going to create a loop that will check wheter the element inside our array is divisible by 5 or not
	let numArr = [5, 12, 30, 46, 40];

	let numDivby5 = [];
	let numNotDivBy5 = [];

	for (let index = 0; index < numArr.length; index++){

		if(numArr[index] % 5 === 0) {
			console.log(numArr[index] + " is divisible by 5");
			numDivby5[numDivby5.length] = numArr[index];
		} else{
			console.log(numArr[index] + " is not divisible by 5");
			numNotDivBy5[numNotDivBy5.length] = numArr[index];
		}
	}

	console.log(numDivby5);
	console.log(numNotDivBy5);

// [Section] Multidimensional Array
		// Multidimensional arrays are useful for storing complex data structure.
		// Though useful in number of cases, creating complex array structure is not always recommneded.

let chessboard = [['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
				  ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
				  ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
				  ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
				  ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
				  ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
				  ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
				  ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
				 ]
console.log(chessboard);

console.log(chessboard[0][2]);
console.log(chessboard[3][4]);

console.table(chessboard);
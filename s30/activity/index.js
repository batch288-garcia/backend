
async function fruitsOnSale(db) {
	return await(

			
			db.fruits.aggregate([
					{$match : {onSale : true }},
					{$count : "kindsOfFruitsOnSale" }
				])
		);
};

async function fruitsInStock(db) {
	return await(

			db.fruits.aggregate([
					{$match : {stock : {$gte : 20}}},
					{$count : "fruitsStockGTE20"}
				])
		);
};

async function fruitsAvePrice(db) {
	return await(

			db.fruits.aggregate([
					{$match : {onSale : true }},
					{$group :
						{
						_id : "$supplier_id",
						averagePricePerSupplier : {$avg : "$price"} 
						}
					},
					{$sort : {_id : 1}},
				])
		);
};

async function fruitsHighPrice(db) {
	return await(

			db.fruits.aggregate([
					{$group :
						{
							_id : "$supplier_id",
							max : {$max : "$price"}
						}
					},
					{$sort : {_id : 1}}
				])

		);
};

async function fruitsLowPrice(db) {
	return await(

		db.fruits.aggregate([
				{$group :
					{
						_id : "$supplier_id",
						min : {$min : "$price"}
					}
				},
				{$sort : {_id : 1}}
			])

		);
}


try{
    module.exports = {
        fruitsOnSale,
        fruitsInStock,
        fruitsAvePrice,
        fruitsHighPrice,
        fruitsLowPrice
    };
} catch(err){

};

// console.log("Hello World!");

// Conditional statements allows us to control the flow of our program. IT allows us to run a statement/instruction if a condition is met or run separate instrruction if otherwise.

// [Section] if, else if and else statement

let numA = -1;

// if staement
	// executes a statement if the specified condition is met or true.
		
	if(numA < 0){
		console.log('Hello');
	}

	/*
		Syntax: 
			if(Condition){
				statement;
			}

	*/

// The result of the expression in the if's conditipn must result to true, else, the statement inside the {} will not run.

	console.log(numA<0);

// Let's update the variable and run an if statement with the same condition;

	numA = 0;

	if (numA < 0) {
		console.log("Hello again if numA is 0!");
	}

	// It will not run because the expression now results to false
	console.log(numA < 0);

	let city = "New York";

	if (city === "New York") {
		console.log("Weolcome to New York City!");

	}

// else if statement
	/*
		-execute a statement if provides previous conditions are false and if the specified condition is trure
		- The 'else if' statement is optional and can added to capture additional conditions to chnage the flow of a program.
	*/

	let numH = 1;

	if (numH > 2) {
		console.log('Hello');
	}
	else if (numH < 2) {
		console.log('World');
	}

// We were abe to run the else if() staement after we evaluated that if condition was false.

	numH = 2;

	if (numH === 2) {
		console.log('Hello');
	}
	else if (numH > 1) {
		console.log('World');
	}

 // else if() statement was no longer run because the if statement was able to run, the evaluation of the whole statement stope there.

	city = "Tokyo";

	if (city === "New York") {
		console.log("Weolcome to New York City!");
	}
	else if (city === "Manila") {
		console.log("Welcome to Manila City, Ph!");
	}
 	else if (city === "Tokyo") {
		console.log("Welcome to Tokyo, Japan!");
	}

	// Since we failed the condition for the if() and the first else if), we went to the secind else f() and checked.

// else statement 

	/*
		-executes a statement if all other conditions are false.
		- The "else" statement is optional and can be added to capture any other possble result to change the flow of a program.

	*/

	let numB = 0;

	if (numB > 0 ) {
		console.log("Hello from numB!");
	}

	else if (numB < 0){
		console.log("World from B!");
	}

	else {
		console.log('Again from numB');
	}

		/*
			Since the preceeding if and else if statement conditions faled the else statement was run instead.
		*/
	// It will casue an errorrrr
	/*	else{
		console.log("Will not run wihtout an if!");
	}*/

	/*	else if (numB === 2){
		console.log("This will cause an error!");
	}*/

// if, else if and else statement wiht functions
	/*
		- most of the times we would like to use if, else if and else statements with functions to contorl the flow of our program.
	*/

	// We are going to crete if a function that will tell the typhoon intensity by providing the wind speed.

	function determineTyphoonIntensity(windSpeed) {
		if (windSpeed < 0) {
			return "Invalid wind speed";
		}
		else if(windSpeed <= 38 && windSpeed >= 0){
			return "Tropical Depression detected!";
		}
		else if(windSpeed >= 39 && windSpeed <= 73){
			return	"Tropical Storm Detected!";
		}
		else if(windSpeed >= 74 && windSpeed <= 95){
			return "Signal Number 1";
		}
		else if(windSpeed >= 96  && windSpeed <= 110){
			return "Signal Number 2";
		}
		else if(windSpeed >= 111 && windSpeed <= 129){
			return	"Signal Number 3";
		}
		else if(windSpeed >= 130 && windSpeed <= 156){
			return "Signal Number 4";
		}
		else{
			return "Signal Number 5";
		}

	}

	console.log(determineTyphoonIntensity(30));

	// console/warn() is a good way to print warnings in our console that could help us developers act on a certain output within our.
	console.warn(determineTyphoonIntensity(40));

// [Section] Truthy anmd Falsy
	// In JavaScript a truthy value is a value that is considered true when encountered in a boolean context.
	// Falsy valuess / Execution for trithy:
	/*
		1. fdalse
		2.0
		3. -0
		4. ""
		5. null
		6. undifiend
		7. NaN
	*/

	// Truthy ex

	if (true) {
		console.log("Truthy");
	}

	if (1) {
		console.log("Truthy");
	}
	
	// Falsy ex
	if (false) {
		console.log("Falsy");
	}

	if (0) {
		console.log("Falsy");
	}

// [Section] Conditional (Ternary) Operator
	/*
	- The Ternary Operatr takes in three operands:
		1. Condition
		2.expression to execute if the condition is truthy
		3. expression to execute if the condition is falsy
	- it can be used to an if else statmet
	- Ternary Operators have an implicit return statement meaning w/o "return" the resulting expression can be stored in a vrauable.

		- Syntax:
			condition ? ifTrue : isFalse;

	*/
	// Single statement execution
	let ternaryResult = (1 < 18) ? true : false;

	console.log("Result of Ternary Operator: " + ternaryResult);

	let exampleTernary = (0) ? "The number is not equal to zero" : "The number is equal to zero";

	console.log(exampleTernary);

	// Multiple Statement Execution
	 function isOfLegalAge() {
	 	let name = "John";
	 	return "You are in the legal age limit, " + name;
	 }

	 function isUnderAge() {
	 	let name = "John";
	 	return "You are under the legal age limit, " + name;
	 }

/*	 let age = parseInt(prompt("What is your age?"));
	 console.log(age);
	 console.log(typeof age);

	 let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();

	 console.log(legalAge);
*/

	 // The parseInt function converts the input received as a sring data type into number

// [Section] Switch statement
// Switch staemet evaluates an expression and metches the expression's value to a case clause. The switch will then execute the statements associated with the case, as well as statements in cases that follow the matching case.

// The break statement is used to terminate the current loop once match has been found

/*
	Syntax:
		switch(expression/variable){
			case value:
				statement;
				break;
			default;
				statement;
				break;
		}

*/

	 // The ".toLowerCase() function will chnage the input received from the prompt into all lowercase letters."
	 let day = prompt("What day of the weel is today?").toLowerCase();

	 switch (day){
	 	case 'monday':
	 		console.log("The color of the day is red!");
	 		break;
	 case 'tuesday':
	 	console.log("The color of the day is orange!");
	 	break;
	 case 'wednesday':
	 	console.log("The color of the day is yellow!");
	 	break;	
	 case 'thursday':
	 	console.log("The color of the day is green!");
	 	break;
	 case 'friday':
	 	console.log("The color of the day is blue!");
	 	break;	
	 case 'saturday':
	 	console.log("The color of the day is indigo!");
	 	break;
	 case 'sunday':
	 	console.log("The color of the day is violet!");
	 	break;	
	 default:
	 	console.log("Please input a valid day!");
	 }

// [Section] Try - catch = Finally Statement
	// try catch statement are commonly used for error hanling
	// There are instance when the application returns an error/warning taht is not necessarily an error in context of our code.

	 function showIntensityAlert(windSpeed) {
	 	try{
	 		alerat(determineTyphoonIntensity(windSpeed));
	 	}
	 	// The catch will only run if and only if there was an erro in the statement inside our try.
	 	catch(error){
	 		console.log(typeof error);

	 		console.warn(error.message);
	 	}
	 	finally{
	 		// Continue execution of code regradless of success and failure of code execution in the try block.
	 		alert('Intensity updates will show new alert!');
	 	}
	 }

	 showIntensityAlert(56);

	 // alerat("Hi");

	 console.log("Hi I;m after the showIntensityAlert");
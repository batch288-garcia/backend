Users
 - ObjectID
 - First Name (string)
 - Las Name (string)
 - Email (string)
 - Password (string)
 - Phone Number (string)
 - Is Admin (Boolean) - if the value of this property is true then user is an admin, otherwise regular customer.
 - Enrollments (Array) - this will be an array hat contains the enrollments of the user.
 
Courses
 - ObjectID (string)
 - Name (string)
 - Price (int)
 - Desription (string)
 - Slots (int)
 - isActive (Boolean) - if true, ut means that the course is available, otherwise not.
 - Enrollees (Array) - that contains the list of students/isers that enrooll the course.

Transactions
 - Transaction ObjectID
 - Course ObjectID
 - user ObjectID
 - isPaid (boolean)
 - Date (Date of transaction)
 - paymentShcem (int)
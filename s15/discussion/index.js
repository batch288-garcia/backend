// single line comment ctrl+/
/*
 multiple lines comment
 ctrl+shift+/ 

 */

// [Section] Syntax, Statements and Comments
// In a programming, it is the set of rules that describes how statments must be constructed. [in math ist like a formula]
// JS statement usually ends w/ semicolon (;) 
// Semicolons are not requred in JS, but we will ue it to help us prepare for the oher strict language like java.
// a syntax in programming, it is the set of rules that descirbes how statement mut be constructed 
// All lines/nlock of code should be written in a specific or else the statement will not run.

// [Section] Variables 
//  It is used to containe/store data.
// Any infrmation taht is used by an application is storedin what we call memory.
// Wehn we create variables, certain portion f a device memory is given a "name" that we call "variables".

// Delcaring variables
// Decalring var it tells our devices that a var name is created and is ready to store data.
	// Syntax:
		// let/const variableName
	
let myVariable;

// by default if u declare and did not initialize its value it will become "undifine"

console.log(myVariable);

/*
	Guides in writing var:
		1. Use te 'let' keyword followed by the variable name of ur choice and use the assignement operators (=) to assign a value.
		2. Variable names should start w/ a lowercase charcter, use camelCase for multiple words.
		3. For constant variables, use 'const' keywords.
		4. Variable names, it should be indicative (descriptive) of the value beng stored to avoid confusion
*/


// decalring and initializing variables
// when we say initializing variables this is the instance when a variable is given its initial or starting value.
	// Syntax
		// let/const variableName = value;


// example
let productName = 'desktop computer';

console.log(productName);

let productPrice = 18999;
console.log(productPrice);

// In the context of certian applications, some variables/information are constant and should not change.
// In this example, the interest rate for a loan or savings account or a mortgage mst not change due to real world concerns.

const interest = 3.359;
console.log(interest);

// Reassigning Variable values
// Reassigning a variable, it means changing it's initial or previous into anoter vallue.
	// Syntax 
		// VariableName = new value;

productName = 'Laptop';
console.log(productName);

// The value of a variabl dealred using the const keyword can't be reassigned.

/*interest = 4.489;
console.log(interest);*/

// Reaasigning variable vs Initilizing Variavbles
// Declares a variable

let	supplier;
// Initializing
supplier = "John Smith Tradings";
// Reassigning
supplier = "Uy's Trading";

// Declaring and initializing a varaible
let	consumer = "Chris";

// Reassigning
consumer = "Topher";

// var vs. let/const keyword
	// var is also used in declaring variables. But var is an EcmaScript 1 version (1997)
	// let/const keyword was introduced as a new features in ES6(2015)
	
// Hoisted pwede gamitin kahit na declare na?
// What makes let/const different from var?
	//  There are issues associated with variables declared/created using var, regarding hoisting
	// Hoisting is JavaScript default behavior of moving declarations to the top.
	//In terms if varibles and constants, keyword var is hoisted and let and const does not allow hoisting.

a = 5;
console.log(a);

var a;

/*b = 6;
console.log(b);
let b;*/

// Scoping
// let/const local or global scope
	// scope essentially means where these variables are available or accessible for use.

	// let and const are block scope by default
	// A block is a chunk of code bounded by {}. A block lives in a curly braces. Anything within the braces are block.

// Globally
let outerVariable = "hello";

let	globalVariable;
	{
		// Locally
		let innerVariable = 'hello again';

		globalVariable = innerVariable;
	}

console.log(globalVariable);

// Multiple variable declaraions and Initialization.
// Multiple variables may be declared in one statement

let productCode = "DC017", productBrand = "Dell";
console.log(productCode);
console.log(productBrand);

// display onelone
console.log(productCode, productBrand);

// Uisnga variable with a reserve keyword

/* Reserve keyword can not be used as a variale name as it has function in javaScript
const let = "Hi Im ;et keyword";
console.log(let);
*/

// [Section] Data Types

// Strings are a series of characters that create a word, a phrase, a sentce or anything related to creating text.
// String in JavaScript can be written using either a single('') or double ("") quote.

let country = 'Philippines';
let	province = "Metro Manila"
	// Concatenation of strings in JavaScrpt
		// Multiple string values can be combined to create a single string using the "+" symbol.

let	fullAddress = province + ', ' + country;
console.log(fullAddress);

let	greetings = 'I live in the ' + country;
console.log(greetings);

// Escape Characters

/*	
	- The escape characters (\) in combinatin with other characters can produce different effects/results.
	- "\n" this creates a next line in between text
*/

let mailAddress = 'Metro Manila \n\nPhilippines'
console.log(mailAddress);

let	message = "John's employees went home early.";
console.log(message);

message = 'John\'s employees went home early.';
console.log(message)

// Numbers
// integers/Whole Numbers
let headcount = 26;
console.log(headcount);

// Decimal Number/Fractions
let	grade = 98.7;
console.log(grade);

// Exponential Notation
let	planetDistance = 2e10;
console.log(planetDistance);

// combining text and strings
// when concanetating strig and int it will convert int to string
console.log("John's grade last quarter is " + grade);

// Boolean
// Boolean values are normally used to create values relteing to the state of certain things.
let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);

// Arrays
// Arrays [] are special kind of data tha's used to store multiple related values.
// Arrays in javscrit can store different data types but is normally used to store similar data types.

// Similar data types
// Syntax:
	// let/const arrayName = [elementA, elementB, elementC, ....]

let grades = [98.7, 92.1, 90.2, 94.6]
console.log(grades);

// different daa types
let details = ["John", "Smith", 32, true];
console.log(details);

// Object
// Objects are another special kind of data type that is use to mimic real world object/items.
// 

/*
	Syntax:
		let/const abjectName = {
			propertyA: valueA,
			propertyB: valueB
		}

*/

let person = {
	fullName:  "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["+63917 123 4567", "8123 45667"],
	address: {
		houseNumber: '345',
		city: 'Manila'
	}
}

console.log(person);

// typeof operator, is used to determine the type of data or value of a variable. It otputs string
console.log(typeof mailAddress);
console.log(typeof headcount);
console.log(typeof isMarried);

console.log(grades);

// Note: Array is a special type of object wih methods and function to manipulate.

// Constant Object and Arrays 
/*
The keyword const is a little misleading

IT does not define a constant value. It Defines a constant reference to a value:

Because of this u can not: 
Reassign a constant value.
Reassign a constant array.
Reassign a constant object.

But you can:

Change the elements of a constabnt array.
Change the properties of constant objects.


*/

const anime = ["One Piece", "One Punch Man", "Attck on Titan"];

/*anime = ["One Piece", "One Punch Man", "Kimetsu no Yaiba"]; */

anime[2] = "Kimetsu no Yaiba";

console.log(anime);

// Null
// It is used to interntionally express the absence of a value in a variable

let spouse = null;

spouse = "Maria";

// Undefined
//undifined diff to Null Represents the sate of a variable that has beed declared but without an assigned value

let fillName;

fillName = "Maria";

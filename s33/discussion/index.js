// console.log("hello");\

// [Section] JavaScript Synchronus vs Asynchronous
	// JS is by default is sycnhronous meaning that only one statement is executed a time.

	// This can be proven when a statement has an error js will not proceed w/ tyhe next statement.

/*console.log("Hello World!");

conole.log("Hello after the world");

console.log("Hello");*/
	
	// When certain staements take a lot of time to process, this slows down our code.

/*for(let index = 0; index <=1500; index++){
	console.log(index);
}

console.log("Hello again!");*/


	// Asychronous mean that we can proceed to executre otehr statements, while time consuming code is running in the background.


// [Section] Getting all posts
	// The Fetch API allows you to asychronously request for a resource data.
	// so it means the fecth method that we are goingf to use her will run asychronously.
	// Syntax:
		// fetch('URL');

	// A "promise" is an object that represents the eventual conmpletion (or failure) of an asynchronous function and it's resulting value.
	console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

	// Syntax:
		/*
			ftch('')
			.then((response => response));
		*/
	// RETRIEVE all posts follow the REST API

	fetch('https://jsonplaceholder.typicode.com/posts')
	// The fecth method will return a promise that resolves the response object.

	// The "then" method captures the respobnse object
	//.then(response => console.log(response));

	// Use the "json" method fropm thereposne object to convert teh datyaretrieved ionto JSON format to be used in our applicaiton
	.then(response => response.json())
	.then(json => console.log(json))


	// The 'async' and 'await' keyword, it is another approach that can be used to achieve asychronous code

	// Creates an asynchronous function 

	async function fetchData() {
	
		let result = await fetch('https://jsonplaceholder.typicode.com/posts');

		console.log(result);

		let json = await result.json()

		console.log(json);

	}

	// fetchData();

	// [Section] Creating specific post

	// Retrieves specific post following the rest API(/posts/:id)

	fetch('https://jsonplaceholder.typicode.com/posts/5')

	.then(response => response.json())

	.then(json => console.log(json));

// [Section] Creating Post
	// Syntax:
		/*
			//options is an object that cintains the method, the heads and the body of the request

			// by default if you dont add the method in the fetch reqquest, it weill be a GET method.

			fetch('URL', {options})
			.then(response => {})
			.then(response => {})
		
		*/

	fetch('https://jsonplaceholder.typicode.com/posts', {
		// sets the method of the request object to post following the rest API
		method: 'POST', 
		// sets the header data of the request object tp be sent to the backend
		// specified that the content will be in JSON structure
		headers: {
			'Content-type' : 'application/json'
		},

		// sets the content/body of the request object to be sent backend
		body: JSON.stringify({
					id: 1,
					title : 'New post',
					body : 'Hello World',
					userID : 1
			})
	})
	.then(response => response.json())
	.then(json => console.log(json));

// [Section] Updae a specific Post


	fetch('https://jsonplaceholder.typicode.com/posts/1',{

		method: "PUT",
		headers: {
			'Content-type' : 'application/json'
		},
		body: JSON.stringify({
					id: 1,
					title: 'Updated Post',
					body: 'Hello again',
					userId: 1
				})
	})

	.then(response => response.json())
	.then(json => console.log(json));

	// PATCH

	fetch('https://jsonplaceholder.typicode.com/posts/1', {

			method: "PATCH",
			headers: {
				'Content-type' : 'application/json'
			},
			body: JSON.stringify({
						title: 'Updated Post',
					})
		})
		.then(response => response.json())
		.then(json => console.log(json))

	fetch('https://jsonplaceholder.typicode.com/posts/1', {

			method: "PUT",
			headers: {
				'Content-type' : 'application/json'
			},
			body: JSON.stringify({	
						title: 'Updated Post',				
					})
		})
		.then(response => response.json())
		.then(json => console.log(json))

		// The PUT Method is a method of modifyung resource where the client sends data that updates the entire object/document

		// PATCH method applies a partial updaet to the object  or document

// [Section] Deleting a post

	fetch('https://jsonplaceholder.typicode.com/posts/1',{
		method: "DELETE"
		})
	.then(response => response.json())
	.then(json => console.log(json));

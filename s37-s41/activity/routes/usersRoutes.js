const express = require("express");

const usersController = require("../controllers/usersControllers.js");

const auth = require("../auth.js");

const router = express.Router();

// Routes
router.post("/register", usersController.registerUser);

// login
router.post("/login", usersController.loginUser);

// getProfile
router.get('/details', auth.verify, usersController.getProfile);

// retrievUser
router.get('/userDetail', auth.verify, usersController.retrieveUserDetails);

// route for course enrollement
router.post("/enroll", auth.verify, usersController.enrollCourse);

// tokenVerification
// router.get("/verify", auth.verify);





module.exports = router;


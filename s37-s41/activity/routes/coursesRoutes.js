// 1.
const express = require("express");



// 2.
const coursesControllers = require('../controllers/coursesControllers.js');

// Activity starts here
const auth = require("../auth.js");
// Activity ends here

// 3.
const router = express.Router();

// WITH PARAMS

// 5.
router.post('/addCourse', auth.verify, coursesControllers.addCourse);

// Route for retrieving all courses
router.get('/', auth.verify, coursesControllers.getAllCourses);

// Route for retrieving all active courses
router.get('/activeCourses', coursesControllers.getActiveCourses);

// Route for inactive courses
router.get("/inactiveCourses", auth.verify, coursesControllers.getInactiveCourses);

// DAPAT MAGKAKASAMA YUNG MAY PAREAMS AT WALA || DAPAT MAUUNA YUGN WALANG PARAMS TAPOS LAHAT NG MAY PARAMS SA BABA KASI IF MAUUNA YUNG MAY PARAMS KESA WALA MAREREQUIRED NA MERON NA DAPAT YUNG NASA BABA


// WITHOUT PARAMS

// Route for getting specific course information
router.get("/:courseId", coursesControllers.getCourse);

// route for updating course
router.patch("/:courseId", auth.verify, coursesControllers.updateCourse);

// Activity starts here
// route for archiving document
router.patch("/:courseId/archive", auth.verify, coursesControllers.archiveCourse);
// Activity ends here


// 4.
module.exports = router;
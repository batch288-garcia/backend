const Users = require("../models/Users.js");
const Courses = require("../models/Courses.js");

const bcrypt = require("bcrypt");

// Require jthe auth.js
const auth = require("../auth.js");

// Controllers here

// Create a contorller for the signup
/*
	Before creating controller Think of its;

	Business Logic/Flow
	
		1. First, we have to validate wheter the user is existing or not. We can do that by validating wheter the email exist or our databases or not.
		2. IF the user email is existing , we will prompt an error telling the uer that the email is taken.
		3. Otherwise, we will sign up or add the user in our database.
*/
module.exports.registerUser = (request, response) => {

	// find method: it will return the an array of object that firs the given criteria

	Users.findOne({email : request.body.email})
	.then(result => {
		// We need add if statement to verify wheter the email exist already in out database

		if(result){
			return response.send(false);
		}else{

			// Create a new Objct instantiated using the Users Model.
			let newUsers = new Users ({
				firstName: request.body.firstName,
				lastName: request.body.lastName,
				email: request.body.email,
				// hashSync method: it hash/encrypt our password
				// the 2nd arguement salt rounds
				password: bcrypt.hashSync(request.body.password, 10),
				mobileNo: request.body.mobileNo
			})

			// Save the user
			// Error handling
			newUsers.save()
			.then(save => response.send(true))
			.catch(error => response.send(false))
		}
	})
	.catch(error => response.send(false));
}

// new controller for the authentiction of the user

module.exports.loginUser = (request, response) => {

	Users.findOne({email : request.body.email})
	.then(result => {

		if (!result) {
			return response.send(false);
		}else{

			// The compareSync method is used to compare a non encrypted password from the login form to the encrypted password retrieve from the find method. returns true pr flase depending the result of the comparison
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);

			if(isPasswordCorrect){ 
				return response.send({auth: auth.createAccessToken(result)});
			}else{
				return response.send(false);
			}
		}
	}).catch(error => response.send(false))
}


module.exports.getProfile = (request, response) => {

		const userData = auth.decode(request.headers.authorization);

		if(userData.isAdmin){

		/*console.log(userData);*/

		Users.findById(request.body.id)
		.then(result => {
			if (result) {

				result.password = "*****"

				return response.send(result)
			}

		}).catch(error => response.send(error));

		}else{
			return	response.send("You are not an admin, you don't have access to this route");
		}
}


// Controller for the enroll course.
module.exports.enrollCourse =  (request, response) => {

	// note pag parmas "_id"
	const courseId = request.body.id;
	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){
		return response.send(false);
	} else{

		// Push papunta kay user document
		let isUSerUpdated =  Users.findOne({_id : userData.id})
		.then(result => {
			result.enrollments.push({
				courseId : courseId
			});

			result.save()
			.then(save => true)
			.catch(error => false)

		})
		.catch(error => false)


		// Push papunta kay course document
		let isCourseUpdated =  Courses.findOne({_id : courseId})
		.then(result => {
			result.enrolles.push({userId: userData.id});

			result.save()
			.then(save => true)
			.catch(error => false)
		})
		.catch(error => false)

		// if condition to check wheter we updated the users document and courses documetn

		if(isUSerUpdated &&  isCourseUpdated){
			return response.send(true);
		} else {
			return response.send(false);
		}


	}

}

module.exports.retrieveUserDetails = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	Users.findOne({_id : userData.id})
	.then(data => response.send(data));
}
const Courses = require("../models/Courses");

// Activity starts here
const auth = require("../auth.js");
// Activity ends here



// This controller is for the Course Creation
module.exports.addCourse =(request, response) => {

// Activity starts here

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){

	// Create an object using the Courses Model
	let newCourse = new Courses({

		// Create
		name: request.body.name,
		description: request.body.description,
		price: request.body.price,
		isActive: request.body.isActive,
		slots: request.body.slots
	})

	newCourse.save()
	.then(save => response.send(true))
	.catch(error => response.send(false));
	}else{
		return response.send(false);
	}
}
// Activity ends here

// In this controller we are goin to retrieve all of the course in our database.
module.exports.getAllCourses = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if(userData.isAdmin){

		Courses.find({})
		.then(result => response.send(result))
		.catch(error => response.send(false));

	}else{
		return response.send(false);
	}
}

// Route for retrieving all active courses
module.exports.getActiveCourses = (request, response) => {
	Courses.find({isActive : true})
	.then(result => response.send(result))
	.catch(error => response.send(false));
}

// Controller will retrieve the information of single document using the priovided params.
module.exports.getCourse = (request, response) => {
	const courseId = request.params.courseId;

	Courses.findById(courseId)
	.then(result => response.send(result))
	.catch(error => response.send(false));
}

// This controller will update our  document/course
module.exports.updateCourse = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;

	// description, price and name

	let updatedCourse = {
		name : request.body.name,
		description : request.body.description,
		price : request.body.price

	}

	if(userData.isAdmin){

		Courses.findByIdAndUpdate(courseId, updatedCourse)
		.then(result => response.send(true))
		.catch(error => response.send(false));

	}else{
		return response.send(false);
	}


}

// Activity starts here
// This controller will archive course

module.exports.archiveCourse = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	const courseId = request.params.courseId;

	let archivedCourse = {
		isActive : request.body.isActive
	}

	if (userData.isAdmin) {
		Courses.findByIdAndUpdate(courseId, archivedCourse)
		.then(result => response.send(true))
		.catch(error => response.send(false)); 
	}else{
		return response.send(false)
	}

}

// Activity ends hereController fo retrievibg archieved  ourses
module.exports.getInactiveCourses = (request, response) => {

	const userData = auth.decode(request.headers.authorization);

	if (!userData.isAdmin) {
		return response.send(false);
	} else{
		Courses.find({isActive : false})
		.then(result => response.send(result))
		.catch(error => response.send(false));
	}

}
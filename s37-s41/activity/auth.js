const jwt = require("jsonwebtoken");

// This is user defined string data that will be used to create our JSON web token.
// Used in the algorithm for encrypting  our data w/c makes it difficult to decode the information w/o the defined keyword.
const secret = "CourseBookingAPI";

// [Section] JSON web token
// Json web token or JWT is a way of securely passing information from the server to the frontend or to the parts of server.
// Information is kept secure through the use of the secret code.

// Functon for token creation 

// Analogy:
	/*
		Pack the gift and provide a lock with the secret code as the key.
	*/

// The argument that will be passes in the parameter will be the document or object  that contains the info of the user.

/*
	User will return or contain the document that is why user."target attribute" 
*/

module.exports.createAccessToken = (user) => {
	const data = {
		id : user._id,
		isAdmin : user.isAdmin,
		email : user.email
	}

	// Generate a JSON web token using JWT's sign method 
	// Generate the token using the form data and the secret code w/ no additional options provided.
	/*
		Syntax:
			return jwt.sign(defined from inside function, defined from above secret, {additional function/options});
	*/
	return jwt.sign(data, secret, {});
}

// Token Verification
/*
	Analogy: 
		Receive gift and opn the lock to verufy if the sender is legitimate and the gift was not tampered.
*/

module.exports.verify = (request, response, next) => {
	let token = request.headers.authorization;


// "Bearer" - index yung 7 sa token
	if(token !== undefined){
		token = token.slice(7, token.length)

		// Validate the token busing the "verify" method in decryptig he token using the secret code.
		return jwt.verify(token, secret, (error, data) => {
			if (error){
				return response.send("false")
			}else{
				next();
			}
		})
	}else{
		return response.send("false");
	}
}

// decode the encrypted token.
module.exports.decode = (token) => {
	token = token.slice(7, token.length);

	// The decode method is used to obtain the information from the JWT
	// The {complete: true} option allows us to return additional informatin from the JWT token


	return jwt.decode(token, {complete: true}
		).payload;
}
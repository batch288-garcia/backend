const express = require("express");
const mongoose = require("mongoose");
const usersRoutes = require("./routes/usersRoutes.js")
const coursesRoutes = require('./routes/coursesRoutes.js')

// It will allow our bakcend applicaitn to be avaialable  to our frointend application
// It will also allows us to control the app's cross origin resources sahring settins.
const cors = require('cors');

const port = 4001;

const app = express();


// Mongo DB connection
// Establish the connection between the DB and the 
// Everytime making new DB chate he courseBooking API
mongoose.connect("mongodb+srv://admin:admin@batch288garcia.ltipebr.mongodb.net/CourseBookingAPI?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Error, can't connect to the db"));

db.once("open", () => console.log("We are now connected to the db"));


// MiddleWare
app.use(express.json());

app.use(express.urlencoded({extended:true}));

// Reminder that we are gong to use this for the sake of the bootcamp
// This not a best practice
app.use(cors());

// add the routing of the routes from the usersRoutes
app.use("/users", usersRoutes);

app.use("/courses", coursesRoutes);


if(require.main === module){
	app.listen(process.env.PORT || 4001, () => {
	    console.log(`API is now online on port ${ process.env.PORT || 4001 }`)
	});
}

module.exports = {app,mongoose};
const express = require('express');

// Mongoose is a package that allows creation of Schemas to Model our data structure
// Also has access to a number of methods for manipulating our database
const mongoose = require('mongoose');

const port = 3001;

const app = express();

	// Section: MongoDB Connection
	// Connect to the database by passing your connection string
	// Due to update in Mngo DB drivers that allow connection, the default connection string is being flagged as an error
	// by default a warning will be displayed in the terminal when the pplication is running.
	// {newUrlParser: true}

	// SYntax:
		/*
			mongoose.connect("MongoDB string", {useNewUrlParser:true});
		*/

	mongoose.connect("mongodb+srv://admin:admin@batch288garcia.ltipebr.mongodb.net/batch288-todo?retryWrites=true&w=majority", {useNewUrlParser: true});

	// Motification wheter the we connected properly with the daabase.

	let db = mongoose.connection;

	// for catching the error just in case we had an error dring the connection
	// console.error.bind allows us to print errors in the browser and in the terminal
	db.on("error", console.error.bind(console, "Error! Can't connect to the Database"));

	// if the connection is successful:
	db.once("open", () => console.log("We're conncted to the cloud database"));

	// [Section] Mongoose Schemas
	// Schemas determine the structure of the document to be written in the database
	// Schemas act as blueprint to our data
	// Use the Schema() constructor of the mongoose module to create a ew Schema object.


	// Middlewares
	// Allows our app to read json data
	app.use(express.json());
	// allows our app to read daa from forms
	app.use(express.urlencoded({extended:true}));




	const taskSchema = new mongoose.Schema({
		// Define the fields with the corresponding data type
		name: String,
		// let us add another field which is status
		status: {
			type: String,
			default: "Pending"
		}
	});

	// [Section] Models
	// Uses schema and are used to create/ instantiate object that correspons to the schema.
	// Modls use Shcema and they act as the middleman from rthe server(JS code) to our database.

	// To create a model we are going to use the model();

	const Task = mongoose.model('Task', taskSchema);

	const AddedTask = mongoose.model('AddedTask', taskSchema);

	// [Section] Routes 

	// Create a POST route to creaete a new task
	// Create a new task
	// Bsiness Logic: 
		/*
			1. Add a functionality to check wheter there are duplicate tasks
				// if the task is existing in the database, we return an error 
				// if the task doesnt exist in the database, we add it in the database.
			2. The task data will be coming from the request's body.
		*/

	app.post("/tasks", (request, response) => {
		// findOne() method is a mongoose method that acts similar to "find" in MongoDB 
		// if the findOne finds a document that matches the criteria it will return the object/document and if there's no object that matches the criteria it will return an empty object or null.
		console.log(request.body);
		Task.findOne({name: request.body.name})
		.then(result => {
			// we can use if statement to check or verify whter we have an object found.
			if(result !== null){
				return response.send("Duplicate task found!");
			}else{

				// create a new task and save it to the dataabse
				let newTask = new Task({
					name: request.body.name
				})

				// the save() method will store the information to the database
				// Since the newTask was creaeted from the Mongoose Schema named Task Model, it will be saved in tasks collection
				newTask.save(); 

				return response.send('New task created!')
			}
		})
	})

	// Get all task in our collection
	//business logic:
		/*
			1. retrieve all the documents
			2. if an error is encountered, print the error
			3. if no error/s is/are found, send a success status to the client and show the documents retreived
		*/

	app.get("/tasks", (request, response) => {

		// The find method is a mongoose method that is similar to MongoDb find
		Task.find({}).then(result => {
			return response.send(result);
		}).catch(error => response.send(error));

	})


// Activity starts Here


// Creating User Schema
	const userSchema = new mongoose.Schema({
	  username: String,
	  password: String
	});

// Creating User Model
	const User = mongoose.model('User', userSchema);

// Creating Post
	app.post("/signup", (request, response) => {

		User.findOne({username: request.body.username})
		.then(usersCollections => {
			if(usersCollections !== null){
				return response.send("Duplicate username found!");
			}else{
				if( request.body.username !== "" && request.body.password !==""){
					let newUser = new User({
						username: request.body.username,
						password: request.body.password
					})

					newUser.save().catch(error => response.send(error));

					return response.send("New user registered")

				}else(request.body.username == "" && request.body.password == "")
					return response.send("BOTH username and password must be provided");
				
			}

		})



	})

	
if(require.main === module){
	app.listen(port, () =>{
		console.log(`Server is running at port ${port}! `);
	})	
}

module.exports = app;

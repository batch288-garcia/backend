const express = require('express');
const mongoose = require('mongoose');
const taskRoutes = require('./routers/taskRoutes.js');

const port = 4000;

const app = express();

// Set up MongoDB connection
mongoose.connect("mongodb+srv://admin:admin@batch288garcia.ltipebr.mongodb.net/batch288-todo?retryWrites=true&w=majority", {useNewUrlParser: true});

// Check wheter we are connected with our db
const db = mongoose.connection;

db.on("error", console.error.bind(console, "Error, can't connect to the db"));

db.once("open", () => console.log("We are now connected to the db!"));

// MiddleWare

app.use(express.json());

app.use(express.urlencoded({extended:true}));

// 
app.use("/tasks", taskRoutes);



if(require.main===module){
	app.listen(port, () => console.log(`The server is running at port ${port}!`));
}

module.exports = app;
const Task = require("../models/task.js");

/*Contorllers*/

// This controller will get/retrieve all the document from the tasks collctions

module.exports.getAllTask = (resquest, response) => {

	Task.find({})
	.then(result => {
		return response.send(result);
	})
	.catch(error => {
		return response.send(error);
	})
}

// Create a contorller that will add a data in our data database

module.exports.addTasks = (request, response) => {

	Task.findOne({"name" : request.body.name})
	.then(result => {
		if(result !== null){
			return response.send("Duplicate Task");
		}else{
			let newTask = new Task({
				"name": request.body.name
			})

			newTask.save();

			return	response.send("New tas created!")
		}
	}).catch(error => response.send(error))

}

module.exports.deleteTask = (request, response) => {
	console.log(request.params.id);

	let taskToBeDeleted = request.params.id

	// In mongoose, we have the findByIDandRemove method that will loook for a document with the same ID provided from the URL and Remove/delete the document from the MongoDB
	Task.findByIdAndRemove(taskToBeDeleted)
	.then(result => {
		return response.send(`The document that has the _id of ${taskToBeDeleted} has been deleted!`)
	}).catch(error => response.send(error));
}

module.exports.getSpecificTask = (request, response) => {
	let taskToGet = request.params.id;

	

	Task.findById(taskToGet)
	.then(result => {
		return response.send(result)
	})
	.catch(error => response.send(error));
}

module.exports.putTaskStatus = (request, response) => {
	
	let taskIdToPut = request.params.id;

	Task.findByIdAndUpdate(taskIdToPut, request.body, {new:true})
	.then(result => {
		return response.send(result);

	}).catch(error => response.send(error));
}
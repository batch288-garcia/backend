const express = require("express");

const taskControllers = require("../controllers/taskControllers.js");

// Contain all the endpoints of our application
const router = express.Router();

router.get("/", taskControllers.getAllTask);

router.post("/addTask", taskControllers.addTasks);

// Parameterized

// We are create a route using a Delete method at the URL "/tasks/:id"

// The colon here is an identifier that helps create a dynamic route which allows us to supply information
router.delete("/:id", taskControllers.deleteTask);


// Activity Starts Here


router.get("/:id", taskControllers.getSpecificTask);

router.put("/:id/complete", taskControllers.putTaskStatus);

module.exports = router;